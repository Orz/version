﻿'use strict';

//var electron = require('electron');
//var app = electron.app;
//var BrowserWindow = electron.BrowserWindow;
//var ipcMain = require('electron').ipcMain;
var mainWindow = null;
var updateWindow = null;
//var Tray = electron.Tray;
var os = require('os');
var path = require('path');
var fs = require('fs');
const { app, BrowserWindow, ipcMain, Tray, net } = require('electron');

global.sharedObject = {
    updateinfo: '',
    version: '',
    dirname: __dirname
}

app.on('window-all-closed', function () {
    if (process.platform != 'darwin') {
        app.quit();
    }
});
app.on('ready', function () {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600
    });
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    //mainWindow.openDevTools();
    mainWindow.on('close', function () {
        if (needUpdate) {
            mainWindow.hide();
            updateWindow = new BrowserWindow({
                width: 500,
                height: 300,
                frame: false,
                alwaysOnTop: true
            });
            updateWindow.on('close', function () {
                updateWindow = null;
            });
            updateWindow.loadURL('file://' + __dirname + '/html/update.html');
            //updateWindow.openDevTools();
            updateWindow.on('closed', function () {
                updateWindow = null;
                mainWindow.destroy();
            });
        } else {
            mainWindow.destroy();
        }
    });
});

var needUpdate = false;

ipcMain.on('getupdateinfo', (event, arg) => {
    needUpdate = true;
    const request = net.request('https://gitlab.com/Orz/version/raw/master/needupdate.json');
    request.on('response', (response) => {
        response.on('data', (res) => {
            console.log(res.toString());
            global.sharedObject.updateinfo = res.toString();
            var versionpath = path.join(__dirname, "\\version.txt");
            fs.writeFileSync(versionpath, arg, 'utf8');
            global.sharedObject.version = arg;
            tray.displayBalloon({ title: '有更新哦~', content: '关闭界面后开始自动更新', icon: __dirname + '/extra/notice.png' });
        });
    });
    request.end();
});

ipcMain.on('signin', (event, arg) => {
    mainWindow.hide();
});

ipcMain.on('closechat', (event, arg) => {
    mainWindow.show();
});


var tray = null;
app.on('ready', () => {
    tray = new Tray(__dirname + '/extra/app.ico');
    tray.setToolTip('开发小工具');
});