﻿var fs = require('fs');
var path = require('path');

$(function () {
    $("#updatecontent").prepend('<p>开始安装更新</p>');
    var version = require('electron').remote.getGlobal('sharedObject').version;
    var dirname = require('electron').remote.getGlobal('sharedObject').dirname;
    var url = 'https://gitlab.com/Orz/version/raw/master/' + version + '/';
    var str = require('electron').remote.getGlobal('sharedObject').updateinfo;
    var updateinfo = JSON.parse(str);
    for (var key in updateinfo) {
        var dir = key;
        var files = updateinfo[key];
        if (dir == 'root') {
            $.each(files, function () {
                var filename = this.valueOf();
                getJson(url + filename, function (data) {
                    fs.writeFileSync(path.join(dirname, "\\", filename), data, 'utf-8');
                    $("#updatecontent").append('<p>更新' + filename + '完成</p>');
                });
            });
        } else {
            $.each(files, function () {
                var filename = this.valueOf();
                getJson(url + dir + '/' + filename, function (data) {
                    fs.writeFileSync(path.join(dirname, "\\", dir, '\\', filename), data, 'utf-8');
                    $("#updatecontent").append('<p>更新' + filename + '完成</p>');
                });
            });
        }
    }
    $("#updatecontent").append('<p>所有更新全部完成</p><a href="#" onclick="javascript:window.close();">close this window</a>');
});

var getJson = function (url, callback) {
    var data = $.ajax({
        url: url,
        async: false
    }).responseText;
    if (callback) {
        callback(data);
    }
}