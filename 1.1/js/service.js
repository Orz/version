var ipcRenderer = require('electron').ipcRenderer;
var fs = require('fs');
var remoteurl = 'https://gitlab.com/Orz/version/raw/master/firstappversion.txt';

$(function () {
    var versionpath = path.join(__dirname, "\\version.txt");
    var olddata = fs.readFileSync(versionpath, 'utf8');
    $.get(remoteurl, function (newdata) {
        if (olddata != newdata) {
            ipcRenderer.send('getupdateinfo', newdata);
        }
    });
});